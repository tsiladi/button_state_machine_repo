**Scope**
Button debuncing with long press detection feature is implemented using finite state machine. 

**State machine**
A state machine is a concept used in designing computer programs or digital logic. There are two types of state machines: finite and infinite state machines. The former is comprised of a finite number of states, transitions, and actions that can be modeled with flow graphs, where the path of logic can be detected when conditions are met. The latter is not practically used.
*Source [techopedia](https://www.techopedia.com/definition/16447/state-machine)



**References**

Coding is done using Visual Studio Code with Arduino extension. Installation of VSC and extension is done using [this tutorial](https://www.codeproject.com/Articles/5150391/Creating-and-Debugging-Arduino-Programs-in-Visual)

Video explanation on which this project is based on can be wached [here](https://www.youtube.com/watch?v=v8KXa5uRavg)