#define DEBUG true
#define Serial if(DEBUG)Serial

#define BUTTON_PIN 5
#define LED_PIN LED_BUILTIN
#define SERIAL_BAUDRATE 115200
#define BUTTON_BOUNCE_DELAY 5
#define LONG_PRESS_THRESHOLD_DELAY 1000

enum buttonStates {
    BUTTON_SM_START,
    BUTTON_SM_GO,
    BUTTON_SM_WAIT,
    BUTTON_SM_ASSERTED,
    BUTTON_SM_DEASSERTED,
    BUTTON_SM_LONG_PRESS_WAIT,
    BUTTON_SM_LONG_PRESS_DETECTED,
    BUTTON_WAIT_TO_BE_DEASSERTED
};
// button state machine initial state
enum buttonStates button_current_state = BUTTON_SM_START;
enum buttonStates button_previous_state = BUTTON_SM_START;

// variable to store button value
int button_value = 0;

// variable to store current time 
unsigned long button_press_t = 0;
// time when timer is started
unsigned long button_press_t0 = 0;

unsigned long button_long_press_t0 = 0;
unsigned long button_long_press_t = 0;


void SM_s1();

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);


  Serial.begin(SERIAL_BAUDRATE);
  Serial.println("Debugging is ON");
}

void loop()
{
  // put your main code here, to run repeatedly:
  SM_s1();

  if (button_current_state == BUTTON_SM_DEASSERTED) Serial.println("DEASSERTED");
  if (button_current_state == BUTTON_SM_ASSERTED) Serial.println("ASSERTED");
  if (button_current_state == BUTTON_SM_LONG_PRESS_DETECTED) Serial.println("Long press detected!");
}

void SM_s1() {
  button_previous_state = button_current_state;
  
  switch (button_current_state) 
  {
    
    case BUTTON_SM_START:
        if (digitalRead(BUTTON_PIN) == LOW) {button_current_state = BUTTON_SM_GO;}
    break;
    
    case BUTTON_SM_GO: 
      button_press_t0 = millis();
      button_current_state = BUTTON_SM_WAIT;
    break;

    case BUTTON_SM_WAIT:
      button_value = digitalRead(BUTTON_PIN);
      button_press_t = millis();
      if(button_value == HIGH) {button_current_state = BUTTON_SM_START;}
      if(button_press_t - button_press_t0 > BUTTON_BOUNCE_DELAY){
        button_current_state = BUTTON_SM_ASSERTED;
        }
    break;

    case BUTTON_SM_ASSERTED: 
      button_value = digitalRead(BUTTON_PIN);
      if (button_value == HIGH) button_current_state = BUTTON_SM_DEASSERTED;
      button_long_press_t0 = millis();
      button_current_state = BUTTON_SM_LONG_PRESS_WAIT;
    break;

    case BUTTON_SM_LONG_PRESS_WAIT:
      button_long_press_t = millis();
      if (button_long_press_t - button_long_press_t0 > LONG_PRESS_THRESHOLD_DELAY) 
      {
        button_current_state = BUTTON_SM_LONG_PRESS_DETECTED;
      }
      if (digitalRead(BUTTON_PIN) == HIGH) button_current_state = BUTTON_SM_DEASSERTED;
    break;

    case BUTTON_SM_LONG_PRESS_DETECTED:
      if (digitalRead(BUTTON_PIN) == HIGH) button_current_state = BUTTON_SM_DEASSERTED;
      button_current_state = BUTTON_WAIT_TO_BE_DEASSERTED;
    break;

    case BUTTON_WAIT_TO_BE_DEASSERTED:
      if (digitalRead(BUTTON_PIN) == HIGH) button_current_state = BUTTON_SM_DEASSERTED;
    break;

    case BUTTON_SM_DEASSERTED:
      button_current_state = BUTTON_SM_START;
    break;
  } 

}